//imagenes
const String imagenJugadores = "assets/images/jugadores.PNG";
const String imagenPartidas = "assets/images/partidas.PNG";
const String imagenLoading = "assets/images/jar-loading.gif";

//imagenes login
const String imagenInicioSesion = "assets/images/ohanami.jpg";
//Mensajes al acceder al sistema (login)
const String mensajeAccesoLogin1 = 'Acceso exitoso';
const String mensajeAccesoLogin2 =
    'El usuario no se encuentra registrado, intente con otro nombre de usuario';
const String mensajeAccesoLogin3 = 'Error de conexión, compruebe su red';
const String mensajeAccesoLogin4 = 'Contrasena incorrecta';

//Mensajes al registrar un nuevo usuario

const String mensajeRegistro1 = 'Registro exitoso';
const String mensajeRegistro2 =
    'El usuario ya existe, intente con otro nombre de usuario';
const String mensajeRegistro3 = 'Error de conexión, compruebe su red';
const String credencialesVacias = "Existen campos vacíos en nombre o contrasena";

//Mensajes al validar los nombres de jugadores
const String camposVacios =
    'Existen campos vacíos en nombre de jugador, intente de nuevo';
const String nombresCorrectos = 'Nombres correctos';
const String nombresRepetidos = "Existen nombres de jugadores repetidos, intente de nuevo";

//Mensaje puntuación de jugadores
const String jugadorYaPuntuado = "El jugador ya ha sido puntuado.";
const String jugadorSinPuntuar = "Faltan jugadores por puntuar.";

//Mensaje subir partida mongo db
const String partidaSubida = "Partida cargada";
const String partidaEliminada = "La Partida ha sido eliminada.";
const String partidaErrorConexion = "Error de conexion";

//Mensaje descargar partidas
const String descargaErrorConexion = "Error de conexion al descargar partidas";
const String eliminarErrorConexion = "Error de conexion al eliminar partida";
const String dataDescargada = "La informacion se descargo con exito";
const String dataVacia = "Error al cargar data";
const String partidaPerdida = "Los datos de la partida han sido perdidos";



//Set de cartas
const int loteLleno = 10;
const int loteVacio = 0;
const int azules = 0;
const int verdes = 1;
const int rosas = 3;
const int negras = 2;

const String ptds = "partidas";

const String r1 = "ronda1";
const String r2 = "ronda2";
const String r3 = "ronda3";

const String jgs = 'jugadores';
const String jgr = 'nombreJugador';

const azl = 'azules';
const vrd = 'verdes';
const ngr = 'negras';
const rss = 'rosas';

//mensaje de advertencia
const String salirDePartida =
    "Mantenga el botón presionado para salir de la partida. Si continua se perderán los datos.";

const String eliminarPartida =
    "Mantenga el botón presionado para eliminar la partida. Los datos de la partida no podrán ser recuperados.";    

const String siguienteRonda =
    "Mantenga presionado el botón para continuar. Asegurarse de puntuar todos los jugadores.";

const String mensajeNombresJug =
    "Mantenga el botón presionado para continuar. Asegurarse de no dejar nombres vacíos, ni repetirlos.";


//mensajes login

//conexion
const String conexionServidor =
    "mongodb+srv://otrousuario:otrousuario@cluster0.qqdjb.mongodb.net/ohanami?retryWrites=true&w=majority";
