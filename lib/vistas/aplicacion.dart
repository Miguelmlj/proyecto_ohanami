import 'package:flutter/material.dart';
import 'package:p_ohanami/vistas/vista_pantallas.dart';

class Aplicacion extends StatelessWidget {
  const Aplicacion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: VistaPantallas(),
    );
  }
}
