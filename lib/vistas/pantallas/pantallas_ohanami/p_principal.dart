import 'package:flutter/material.dart';
import 'package:p_ohanami/app/constantes.dart';
import 'package:p_ohanami/bloc/bloc.dart';
import 'package:p_ohanami/bloc/eventos.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PantallaPrincipal extends StatelessWidget {
  const PantallaPrincipal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: Center(child: Text('Bienvenido a Ohanami')),
            backgroundColor: Colors.black54,
            actions: [CerrarSesion()],
          ),
          body: const CuerpoPrincipal(),
        ));
  }
}

class CerrarSesion extends StatelessWidget {
  const CerrarSesion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: ChoiceChip(
        label: Text('Salir'),
        selected: false,
        onSelected: (select) {
          context.read<BlocUno>().add(CerrandoSesion());
        },
      ),
    ));
  }
}

class CuerpoPrincipal extends StatelessWidget {
  const CuerpoPrincipal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(height: 20),
          Center(
              child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Seleccione la acción que desea realizar:',
              style: TextStyle(color: Colors.black54, fontSize: 20),
            ),
          )),
          SizedBox(height: 10),
          Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)),
              margin: EdgeInsets.all(15),
              elevation: 10,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30),
                child: Column(
                  children: <Widget>[
                    FadeInImage(
                      image: AssetImage(imagenPartidas),
                      placeholder: AssetImage(imagenLoading),
                      fit: BoxFit.cover,
                      height: 260,
                    ),
                    //
                  ],
                ),
              )),
          SizedBox(height: 10),
          Center(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Colors.red.shade300,
                  onPrimary: Colors.white,
                  shadowColor: Colors.teal,
                  elevation: 5,
                  textStyle: const TextStyle(
                    fontSize: 30,
                    color: Colors.black,
                    fontStyle: FontStyle.italic,
                  )),
              onPressed: () {
                context.read<BlocUno>().add(AbrirNuevaPartida());
              },
              child: const Text('Empezar nueva partida'),
            ),
          ),
          SizedBox(height: 20),
          Center(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.black54,
                onPrimary: Colors.white,
                shadowColor: Colors.teal,
                elevation: 5,
                textStyle: const TextStyle(
                  fontSize: 30,
                  color: Colors.black,
                  fontStyle: FontStyle.italic,
                ),
              ),
              onPressed: () {
                context.read<BlocUno>().add(AbrirListaPartidas());
              },
              child: const Text('Ver mis partidas'),
            ),
          ),
        ],
      ),
    );
  }
}
