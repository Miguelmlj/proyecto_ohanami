import 'package:flutter/material.dart';


import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class PantallaFinalizandoPartida extends StatelessWidget {
  const PantallaFinalizandoPartida({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Center(child: Text('Pantalla de Espera')),
          backgroundColor: Colors.black54),
      body: Procesando(),
    );
  }
}

class Procesando extends StatelessWidget {
  const Procesando({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    context.read<BlocUno>().add(ResultadosObtenidosFinPart());

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Container(
                width: 200, height: 200, child: CircularProgressIndicator(
                  color: Colors.red.shade300,
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Center(
              child: Text(
                'Finalizando Partida',
                 style: estiloMensaje
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: ListTile(
              title: Center(child: Text('El proceso tardará algunos segundos', style: estiloMensaje)),
              subtitle: Center(
                  child: Text('Los resultados serán mostrados en un momento')),
            ),
          )
        ],
      ),
    );
  }
}
