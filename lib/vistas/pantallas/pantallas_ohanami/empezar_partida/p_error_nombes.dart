import 'package:flutter/material.dart';


import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class PantallaErrorNombres extends StatelessWidget {
  const PantallaErrorNombres(this.mensajeError,{Key? key}) : super(key: key);
  final String mensajeError;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Error en nombres')),
        backgroundColor: Colors.black54,
      ),
      body: ErrorC(mensajeError),
    );
  }
}

class ErrorC extends StatelessWidget {
  const ErrorC(this.mensajeError,{Key? key}) : super(key: key);
  final String mensajeError;

  @override
  Widget build(BuildContext context) {
    
    return Container(
        child: Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Text(
              mensajeError,
              style: estiloMensaje,
            ),
          ),
        ),
        SizedBox(height: 20.0),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: Colors.black54,
              onPrimary: Colors.white,
              shadowColor: Colors.teal,
              elevation: 5,
              textStyle: const TextStyle(fontSize: 20)),
          onPressed: () {
            context.read<BlocUno>().add(AbrirNombrarJugadores());
          },
          child: const Text('Intentar de nuevo'),
        ),
      ],
    ));
  }
}
