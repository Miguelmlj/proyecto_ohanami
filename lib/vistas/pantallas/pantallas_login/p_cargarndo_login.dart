import 'package:flutter/material.dart';
import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class PantallaIniciandoSesion extends StatelessWidget {
  const PantallaIniciandoSesion(this.nombre, this.contrasena,{Key? key}) : super(key: key);
  final String nombre;
  final String contrasena;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Center(child: Text('Login')), backgroundColor: Colors.black54),
      body: CargandoLogin(nombre, contrasena),
    );
  }
}

class CargandoLogin extends StatelessWidget {
  const CargandoLogin(this.nombre, this.contrasena,{Key? key}) : super(key: key);
  final String nombre;
  final String contrasena;

  @override
  Widget build(BuildContext context) {
    
    context.read<BlocUno>().add(IniciarSesion(nombre: nombre, contrasena: contrasena));

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Container(
                width: 200, height: 200, child: CircularProgressIndicator(
                  color: Colors.red.shade300,
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Center(
              child: Text(
                'Iniciando', style: estiloMensaje,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: ListTile(
              title: Center(
                  child: Text('El proceso puede tardar algunos segundos', style: estiloMensaje,)),
              subtitle: Center(child: Text('Será redirigido automáticamente')),
            ),
          )
        ],
      ),
    );
  }
}
