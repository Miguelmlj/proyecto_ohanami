import 'package:flutter/material.dart';
import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class PantallaErrorAcceso extends StatelessWidget {
  const PantallaErrorAcceso(this.respuestaLogin, {Key? key}) : super(key: key);

  final String respuestaLogin;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Error de acceso')),
        backgroundColor: Colors.black54,
      ),
      body: CuerpoError(respuestaLogin),
    );
  }
}

class CuerpoError extends StatelessWidget {
  const CuerpoError(this.respuesta, {Key? key}) : super(key: key);
  final String respuesta;
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        SizedBox(height: 30),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Text(
              respuesta,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0,
                  color: const Color(0xFF616161)),
            ),
          ),
        ),
        SizedBox(
          height: 50.0,
        ),
        Icon(Icons.error_outline, size: 70, color: Colors.grey),
        SizedBox(height: 20),
        Center(
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Colors.black54,
                onPrimary: Colors.white,
                shadowColor: Colors.teal,
                elevation: 5,
                textStyle: const TextStyle(fontSize: 20)),
            onPressed: () {
              context.read<BlocUno>().add(AbrirLogin());
            },
            child: const Text('Intentar de nuevo'),
          ),
        ),
        SizedBox(height: 20),
        Center(
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Colors.black54,
                onPrimary: Colors.white,
                shadowColor: Colors.teal,
                elevation: 5,
                textStyle: const TextStyle(fontSize: 20)),
            onPressed: () {
              context.read<BlocUno>().add(AbrirRegistro());
            },
            child: const Text('Registrar nuevo usuario'),
          ),
        ),
      ],
    ));
  }
}
