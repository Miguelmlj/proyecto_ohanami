import 'package:flutter/material.dart';
import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class PantallaRespRegistro extends StatelessWidget {
  const PantallaRespRegistro(this.respuestaRegistro,{Key? key}) : super(key: key);
  final String respuestaRegistro;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Resultados de Registro')),
        backgroundColor: Colors.black54,
      ),
      body: CuerpoRespRegistro(respuestaRegistro),
    );
  }
}

class CuerpoRespRegistro extends StatelessWidget {
  const CuerpoRespRegistro(this.respuestaRegistro,{Key? key}) : super(key: key);
  final String respuestaRegistro;

  @override
  Widget build(BuildContext context) {
    
    return Container(
      child: Column(
        children: [
          SizedBox(height: 50),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Text(respuestaRegistro,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                      color: const Color(0xFF616161))),
            ),
          ),
          SizedBox(
            height: 50.0,
          ),
          Icon(Icons.message, size: 70, color: Colors.grey),
          SizedBox(height: 20),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Colors.black54,
                onPrimary: Colors.white,
                shadowColor: Colors.teal,
                elevation: 5,
                textStyle: const TextStyle(fontSize: 20)),
            onPressed: () {
              context.read<BlocUno>().add(AbrirLogin());
            },
            child: const Text('Ir a login'),
          ),
          SizedBox(height: 20),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Colors.black54,
                onPrimary: Colors.white,
                shadowColor: Colors.teal,
                elevation: 5,
                textStyle: const TextStyle(fontSize: 20)),
            onPressed: () {
              context.read<BlocUno>().add(AbrirRegistro());
            },
            child: const Text('Intentar de nuevo'),
          ),
        ],
      ),
    );
  }
}
