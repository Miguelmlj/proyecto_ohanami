library p_ohanami;

export 'package:p_ohanami/vistas/pantallas/pantallas_login/p_cargarndo_login.dart';
export 'package:p_ohanami/vistas/pantallas/pantallas_login/p_login.dart';

export 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/empezar_partida/p_carg_fin_partida.dart';

export 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/empezar_partida/p_error_jug_puntuado.dart';
export 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/empezar_partida/p_error_nombes.dart';
export 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/empezar_partida/p_nombrar.dart';

export 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/empezar_partida/p_puntuacion_cartasr1.dart';

export 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/empezar_partida/p_result_partida.dart';

export 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/empezar_partida/p_ronda1.dart';

export 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/empezar_partida/p_seleccion_jugadores.dart';
export 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/lista_partidas/detalle_partida.dart';

export 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/lista_partidas/grafica_rondas_jug.dart';

export 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/lista_partidas/partidas_usuario.dart';
export 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_principal.dart';
export 'package:p_ohanami/vistas/pantallas/pantallas_registro/p_carg_nuevo_reg.dart';
export 'package:p_ohanami/vistas/pantallas/pantallas_registro/p_registro.dart';
export 'package:p_ohanami/vistas/pantallas/pantallas_login/p_error_acceso.dart';
export 'package:p_ohanami/vistas/pantallas/pantallas_registro/p_resp_registro.dart';
export 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/empezar_partida/p_carga_err_partida.dart';
