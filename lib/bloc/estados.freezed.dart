// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'estados.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$EstadoTearOff {
  const _$EstadoTearOff();

  Login call() {
    return const Login();
  }

  Iniciandome iniciandome(String nombre, String contrasena) {
    return Iniciandome(
      nombre,
      contrasena,
    );
  }

  PaginaPrincipal paginaprincipal() {
    return const PaginaPrincipal();
  }

  PaginaRegistro paginaregistro() {
    return const PaginaRegistro();
  }

  Registrandome registrandome(String nombre, String contrasena) {
    return Registrandome(
      nombre,
      contrasena,
    );
  }

  PaginaErrorAcceso paginaerroracceso(String mensaje) {
    return PaginaErrorAcceso(
      mensaje,
    );
  }

  PaginaRespRegistro paginarespregistro(String mensajeRegistro) {
    return PaginaRespRegistro(
      mensajeRegistro,
    );
  }

  PaginaNuevaPartida paginanuevapartida() {
    return const PaginaNuevaPartida();
  }

  PaginaNombrarJugadores paginanombrarjugadores(
      int cantidadJugadores, List<String> nombreJugadores) {
    return PaginaNombrarJugadores(
      cantidadJugadores,
      nombreJugadores,
    );
  }

  PaginaRondaPrimera paginarondaprimera(
      List<String> listaConJugadores, int ronda) {
    return PaginaRondaPrimera(
      listaConJugadores,
      ronda,
    );
  }

  ErrorNombresJugadores errornombresjugadores(String mensajeValidacion) {
    return ErrorNombresJugadores(
      mensajeValidacion,
    );
  }

  PaginaCartasJugadorR1 paginacartasjugadorr1(List<int> numeroDeCartas,
      String nombreJugador, int cantidadCartasRepartir, int ronda) {
    return PaginaCartasJugadorR1(
      numeroDeCartas,
      nombreJugador,
      cantidadCartasRepartir,
      ronda,
    );
  }

  ErrorJugadorSeleccionado errorjugadorseleccionado(String msj) {
    return ErrorJugadorSeleccionado(
      msj,
    );
  }

  CargandoPartidaTerminada cargandopartidaterminada() {
    return const CargandoPartidaTerminada();
  }

  ErrorConexionPartidaT errorconexionpartidat(String msj, String msj2) {
    return ErrorConexionPartidaT(
      msj,
      msj2,
    );
  }

  PartidaTerminada partidaterminada(List<dynamic> partidaTerminada) {
    return PartidaTerminada(
      partidaTerminada,
    );
  }

  PaginaMisPartidas paginamispartidas(List<dynamic> misPartidas) {
    return PaginaMisPartidas(
      misPartidas,
    );
  }

  VerDetallesPartida verdetallespartida(
      List<dynamic> detallePartida, int index) {
    return VerDetallesPartida(
      detallePartida,
      index,
    );
  }

  EstadisticaJugadorMostrada estadisticajugadormostrada(
      List<dynamic> lista, int indice, int index) {
    return EstadisticaJugadorMostrada(
      lista,
      indice,
      index,
    );
  }

  EliminandoPartida eliminandopartida(int indice) {
    return EliminandoPartida(
      indice,
    );
  }

  PartidaEliminada partidaeliminada(String mensaje) {
    return PartidaEliminada(
      mensaje,
    );
  }
}

/// @nodoc
const $Estado = _$EstadoTearOff();

/// @nodoc
mixin _$Estado {
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EstadoCopyWith<$Res> {
  factory $EstadoCopyWith(Estado value, $Res Function(Estado) then) =
      _$EstadoCopyWithImpl<$Res>;
}

/// @nodoc
class _$EstadoCopyWithImpl<$Res> implements $EstadoCopyWith<$Res> {
  _$EstadoCopyWithImpl(this._value, this._then);

  final Estado _value;
  // ignore: unused_field
  final $Res Function(Estado) _then;
}

/// @nodoc
abstract class $LoginCopyWith<$Res> {
  factory $LoginCopyWith(Login value, $Res Function(Login) then) =
      _$LoginCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoginCopyWithImpl<$Res> extends _$EstadoCopyWithImpl<$Res>
    implements $LoginCopyWith<$Res> {
  _$LoginCopyWithImpl(Login _value, $Res Function(Login) _then)
      : super(_value, (v) => _then(v as Login));

  @override
  Login get _value => super._value as Login;
}

/// @nodoc

class _$Login with DiagnosticableTreeMixin implements Login {
  const _$Login();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'Estado'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is Login);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return $default();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return $default?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if ($default != null) {
      return $default();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return $default(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return $default?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if ($default != null) {
      return $default(this);
    }
    return orElse();
  }
}

abstract class Login implements Estado {
  const factory Login() = _$Login;
}

/// @nodoc
abstract class $IniciandomeCopyWith<$Res> {
  factory $IniciandomeCopyWith(
          Iniciandome value, $Res Function(Iniciandome) then) =
      _$IniciandomeCopyWithImpl<$Res>;
  $Res call({String nombre, String contrasena});
}

/// @nodoc
class _$IniciandomeCopyWithImpl<$Res> extends _$EstadoCopyWithImpl<$Res>
    implements $IniciandomeCopyWith<$Res> {
  _$IniciandomeCopyWithImpl(
      Iniciandome _value, $Res Function(Iniciandome) _then)
      : super(_value, (v) => _then(v as Iniciandome));

  @override
  Iniciandome get _value => super._value as Iniciandome;

  @override
  $Res call({
    Object? nombre = freezed,
    Object? contrasena = freezed,
  }) {
    return _then(Iniciandome(
      nombre == freezed
          ? _value.nombre
          : nombre // ignore: cast_nullable_to_non_nullable
              as String,
      contrasena == freezed
          ? _value.contrasena
          : contrasena // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$Iniciandome with DiagnosticableTreeMixin implements Iniciandome {
  const _$Iniciandome(this.nombre, this.contrasena);

  @override
  final String nombre;
  @override
  final String contrasena;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.iniciandome(nombre: $nombre, contrasena: $contrasena)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.iniciandome'))
      ..add(DiagnosticsProperty('nombre', nombre))
      ..add(DiagnosticsProperty('contrasena', contrasena));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is Iniciandome &&
            (identical(other.nombre, nombre) ||
                const DeepCollectionEquality().equals(other.nombre, nombre)) &&
            (identical(other.contrasena, contrasena) ||
                const DeepCollectionEquality()
                    .equals(other.contrasena, contrasena)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(nombre) ^
      const DeepCollectionEquality().hash(contrasena);

  @JsonKey(ignore: true)
  @override
  $IniciandomeCopyWith<Iniciandome> get copyWith =>
      _$IniciandomeCopyWithImpl<Iniciandome>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return iniciandome(nombre, contrasena);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return iniciandome?.call(nombre, contrasena);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (iniciandome != null) {
      return iniciandome(nombre, contrasena);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return iniciandome(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return iniciandome?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (iniciandome != null) {
      return iniciandome(this);
    }
    return orElse();
  }
}

abstract class Iniciandome implements Estado {
  const factory Iniciandome(String nombre, String contrasena) = _$Iniciandome;

  String get nombre => throw _privateConstructorUsedError;
  String get contrasena => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $IniciandomeCopyWith<Iniciandome> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PaginaPrincipalCopyWith<$Res> {
  factory $PaginaPrincipalCopyWith(
          PaginaPrincipal value, $Res Function(PaginaPrincipal) then) =
      _$PaginaPrincipalCopyWithImpl<$Res>;
}

/// @nodoc
class _$PaginaPrincipalCopyWithImpl<$Res> extends _$EstadoCopyWithImpl<$Res>
    implements $PaginaPrincipalCopyWith<$Res> {
  _$PaginaPrincipalCopyWithImpl(
      PaginaPrincipal _value, $Res Function(PaginaPrincipal) _then)
      : super(_value, (v) => _then(v as PaginaPrincipal));

  @override
  PaginaPrincipal get _value => super._value as PaginaPrincipal;
}

/// @nodoc

class _$PaginaPrincipal
    with DiagnosticableTreeMixin
    implements PaginaPrincipal {
  const _$PaginaPrincipal();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.paginaprincipal()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'Estado.paginaprincipal'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is PaginaPrincipal);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return paginaprincipal();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return paginaprincipal?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginaprincipal != null) {
      return paginaprincipal();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return paginaprincipal(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return paginaprincipal?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginaprincipal != null) {
      return paginaprincipal(this);
    }
    return orElse();
  }
}

abstract class PaginaPrincipal implements Estado {
  const factory PaginaPrincipal() = _$PaginaPrincipal;
}

/// @nodoc
abstract class $PaginaRegistroCopyWith<$Res> {
  factory $PaginaRegistroCopyWith(
          PaginaRegistro value, $Res Function(PaginaRegistro) then) =
      _$PaginaRegistroCopyWithImpl<$Res>;
}

/// @nodoc
class _$PaginaRegistroCopyWithImpl<$Res> extends _$EstadoCopyWithImpl<$Res>
    implements $PaginaRegistroCopyWith<$Res> {
  _$PaginaRegistroCopyWithImpl(
      PaginaRegistro _value, $Res Function(PaginaRegistro) _then)
      : super(_value, (v) => _then(v as PaginaRegistro));

  @override
  PaginaRegistro get _value => super._value as PaginaRegistro;
}

/// @nodoc

class _$PaginaRegistro with DiagnosticableTreeMixin implements PaginaRegistro {
  const _$PaginaRegistro();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.paginaregistro()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'Estado.paginaregistro'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is PaginaRegistro);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return paginaregistro();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return paginaregistro?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginaregistro != null) {
      return paginaregistro();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return paginaregistro(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return paginaregistro?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginaregistro != null) {
      return paginaregistro(this);
    }
    return orElse();
  }
}

abstract class PaginaRegistro implements Estado {
  const factory PaginaRegistro() = _$PaginaRegistro;
}

/// @nodoc
abstract class $RegistrandomeCopyWith<$Res> {
  factory $RegistrandomeCopyWith(
          Registrandome value, $Res Function(Registrandome) then) =
      _$RegistrandomeCopyWithImpl<$Res>;
  $Res call({String nombre, String contrasena});
}

/// @nodoc
class _$RegistrandomeCopyWithImpl<$Res> extends _$EstadoCopyWithImpl<$Res>
    implements $RegistrandomeCopyWith<$Res> {
  _$RegistrandomeCopyWithImpl(
      Registrandome _value, $Res Function(Registrandome) _then)
      : super(_value, (v) => _then(v as Registrandome));

  @override
  Registrandome get _value => super._value as Registrandome;

  @override
  $Res call({
    Object? nombre = freezed,
    Object? contrasena = freezed,
  }) {
    return _then(Registrandome(
      nombre == freezed
          ? _value.nombre
          : nombre // ignore: cast_nullable_to_non_nullable
              as String,
      contrasena == freezed
          ? _value.contrasena
          : contrasena // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$Registrandome with DiagnosticableTreeMixin implements Registrandome {
  const _$Registrandome(this.nombre, this.contrasena);

  @override
  final String nombre;
  @override
  final String contrasena;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.registrandome(nombre: $nombre, contrasena: $contrasena)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.registrandome'))
      ..add(DiagnosticsProperty('nombre', nombre))
      ..add(DiagnosticsProperty('contrasena', contrasena));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is Registrandome &&
            (identical(other.nombre, nombre) ||
                const DeepCollectionEquality().equals(other.nombre, nombre)) &&
            (identical(other.contrasena, contrasena) ||
                const DeepCollectionEquality()
                    .equals(other.contrasena, contrasena)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(nombre) ^
      const DeepCollectionEquality().hash(contrasena);

  @JsonKey(ignore: true)
  @override
  $RegistrandomeCopyWith<Registrandome> get copyWith =>
      _$RegistrandomeCopyWithImpl<Registrandome>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return registrandome(nombre, contrasena);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return registrandome?.call(nombre, contrasena);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (registrandome != null) {
      return registrandome(nombre, contrasena);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return registrandome(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return registrandome?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (registrandome != null) {
      return registrandome(this);
    }
    return orElse();
  }
}

abstract class Registrandome implements Estado {
  const factory Registrandome(String nombre, String contrasena) =
      _$Registrandome;

  String get nombre => throw _privateConstructorUsedError;
  String get contrasena => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RegistrandomeCopyWith<Registrandome> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PaginaErrorAccesoCopyWith<$Res> {
  factory $PaginaErrorAccesoCopyWith(
          PaginaErrorAcceso value, $Res Function(PaginaErrorAcceso) then) =
      _$PaginaErrorAccesoCopyWithImpl<$Res>;
  $Res call({String mensaje});
}

/// @nodoc
class _$PaginaErrorAccesoCopyWithImpl<$Res> extends _$EstadoCopyWithImpl<$Res>
    implements $PaginaErrorAccesoCopyWith<$Res> {
  _$PaginaErrorAccesoCopyWithImpl(
      PaginaErrorAcceso _value, $Res Function(PaginaErrorAcceso) _then)
      : super(_value, (v) => _then(v as PaginaErrorAcceso));

  @override
  PaginaErrorAcceso get _value => super._value as PaginaErrorAcceso;

  @override
  $Res call({
    Object? mensaje = freezed,
  }) {
    return _then(PaginaErrorAcceso(
      mensaje == freezed
          ? _value.mensaje
          : mensaje // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$PaginaErrorAcceso
    with DiagnosticableTreeMixin
    implements PaginaErrorAcceso {
  const _$PaginaErrorAcceso(this.mensaje);

  @override
  final String mensaje;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.paginaerroracceso(mensaje: $mensaje)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.paginaerroracceso'))
      ..add(DiagnosticsProperty('mensaje', mensaje));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is PaginaErrorAcceso &&
            (identical(other.mensaje, mensaje) ||
                const DeepCollectionEquality().equals(other.mensaje, mensaje)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(mensaje);

  @JsonKey(ignore: true)
  @override
  $PaginaErrorAccesoCopyWith<PaginaErrorAcceso> get copyWith =>
      _$PaginaErrorAccesoCopyWithImpl<PaginaErrorAcceso>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return paginaerroracceso(mensaje);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return paginaerroracceso?.call(mensaje);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginaerroracceso != null) {
      return paginaerroracceso(mensaje);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return paginaerroracceso(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return paginaerroracceso?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginaerroracceso != null) {
      return paginaerroracceso(this);
    }
    return orElse();
  }
}

abstract class PaginaErrorAcceso implements Estado {
  const factory PaginaErrorAcceso(String mensaje) = _$PaginaErrorAcceso;

  String get mensaje => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PaginaErrorAccesoCopyWith<PaginaErrorAcceso> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PaginaRespRegistroCopyWith<$Res> {
  factory $PaginaRespRegistroCopyWith(
          PaginaRespRegistro value, $Res Function(PaginaRespRegistro) then) =
      _$PaginaRespRegistroCopyWithImpl<$Res>;
  $Res call({String mensajeRegistro});
}

/// @nodoc
class _$PaginaRespRegistroCopyWithImpl<$Res> extends _$EstadoCopyWithImpl<$Res>
    implements $PaginaRespRegistroCopyWith<$Res> {
  _$PaginaRespRegistroCopyWithImpl(
      PaginaRespRegistro _value, $Res Function(PaginaRespRegistro) _then)
      : super(_value, (v) => _then(v as PaginaRespRegistro));

  @override
  PaginaRespRegistro get _value => super._value as PaginaRespRegistro;

  @override
  $Res call({
    Object? mensajeRegistro = freezed,
  }) {
    return _then(PaginaRespRegistro(
      mensajeRegistro == freezed
          ? _value.mensajeRegistro
          : mensajeRegistro // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$PaginaRespRegistro
    with DiagnosticableTreeMixin
    implements PaginaRespRegistro {
  const _$PaginaRespRegistro(this.mensajeRegistro);

  @override
  final String mensajeRegistro;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.paginarespregistro(mensajeRegistro: $mensajeRegistro)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.paginarespregistro'))
      ..add(DiagnosticsProperty('mensajeRegistro', mensajeRegistro));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is PaginaRespRegistro &&
            (identical(other.mensajeRegistro, mensajeRegistro) ||
                const DeepCollectionEquality()
                    .equals(other.mensajeRegistro, mensajeRegistro)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(mensajeRegistro);

  @JsonKey(ignore: true)
  @override
  $PaginaRespRegistroCopyWith<PaginaRespRegistro> get copyWith =>
      _$PaginaRespRegistroCopyWithImpl<PaginaRespRegistro>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return paginarespregistro(mensajeRegistro);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return paginarespregistro?.call(mensajeRegistro);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginarespregistro != null) {
      return paginarespregistro(mensajeRegistro);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return paginarespregistro(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return paginarespregistro?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginarespregistro != null) {
      return paginarespregistro(this);
    }
    return orElse();
  }
}

abstract class PaginaRespRegistro implements Estado {
  const factory PaginaRespRegistro(String mensajeRegistro) =
      _$PaginaRespRegistro;

  String get mensajeRegistro => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PaginaRespRegistroCopyWith<PaginaRespRegistro> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PaginaNuevaPartidaCopyWith<$Res> {
  factory $PaginaNuevaPartidaCopyWith(
          PaginaNuevaPartida value, $Res Function(PaginaNuevaPartida) then) =
      _$PaginaNuevaPartidaCopyWithImpl<$Res>;
}

/// @nodoc
class _$PaginaNuevaPartidaCopyWithImpl<$Res> extends _$EstadoCopyWithImpl<$Res>
    implements $PaginaNuevaPartidaCopyWith<$Res> {
  _$PaginaNuevaPartidaCopyWithImpl(
      PaginaNuevaPartida _value, $Res Function(PaginaNuevaPartida) _then)
      : super(_value, (v) => _then(v as PaginaNuevaPartida));

  @override
  PaginaNuevaPartida get _value => super._value as PaginaNuevaPartida;
}

/// @nodoc

class _$PaginaNuevaPartida
    with DiagnosticableTreeMixin
    implements PaginaNuevaPartida {
  const _$PaginaNuevaPartida();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.paginanuevapartida()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'Estado.paginanuevapartida'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is PaginaNuevaPartida);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return paginanuevapartida();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return paginanuevapartida?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginanuevapartida != null) {
      return paginanuevapartida();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return paginanuevapartida(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return paginanuevapartida?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginanuevapartida != null) {
      return paginanuevapartida(this);
    }
    return orElse();
  }
}

abstract class PaginaNuevaPartida implements Estado {
  const factory PaginaNuevaPartida() = _$PaginaNuevaPartida;
}

/// @nodoc
abstract class $PaginaNombrarJugadoresCopyWith<$Res> {
  factory $PaginaNombrarJugadoresCopyWith(PaginaNombrarJugadores value,
          $Res Function(PaginaNombrarJugadores) then) =
      _$PaginaNombrarJugadoresCopyWithImpl<$Res>;
  $Res call({int cantidadJugadores, List<String> nombreJugadores});
}

/// @nodoc
class _$PaginaNombrarJugadoresCopyWithImpl<$Res>
    extends _$EstadoCopyWithImpl<$Res>
    implements $PaginaNombrarJugadoresCopyWith<$Res> {
  _$PaginaNombrarJugadoresCopyWithImpl(PaginaNombrarJugadores _value,
      $Res Function(PaginaNombrarJugadores) _then)
      : super(_value, (v) => _then(v as PaginaNombrarJugadores));

  @override
  PaginaNombrarJugadores get _value => super._value as PaginaNombrarJugadores;

  @override
  $Res call({
    Object? cantidadJugadores = freezed,
    Object? nombreJugadores = freezed,
  }) {
    return _then(PaginaNombrarJugadores(
      cantidadJugadores == freezed
          ? _value.cantidadJugadores
          : cantidadJugadores // ignore: cast_nullable_to_non_nullable
              as int,
      nombreJugadores == freezed
          ? _value.nombreJugadores
          : nombreJugadores // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc

class _$PaginaNombrarJugadores
    with DiagnosticableTreeMixin
    implements PaginaNombrarJugadores {
  const _$PaginaNombrarJugadores(this.cantidadJugadores, this.nombreJugadores);

  @override
  final int cantidadJugadores;
  @override
  final List<String> nombreJugadores;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.paginanombrarjugadores(cantidadJugadores: $cantidadJugadores, nombreJugadores: $nombreJugadores)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.paginanombrarjugadores'))
      ..add(DiagnosticsProperty('cantidadJugadores', cantidadJugadores))
      ..add(DiagnosticsProperty('nombreJugadores', nombreJugadores));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is PaginaNombrarJugadores &&
            (identical(other.cantidadJugadores, cantidadJugadores) ||
                const DeepCollectionEquality()
                    .equals(other.cantidadJugadores, cantidadJugadores)) &&
            (identical(other.nombreJugadores, nombreJugadores) ||
                const DeepCollectionEquality()
                    .equals(other.nombreJugadores, nombreJugadores)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(cantidadJugadores) ^
      const DeepCollectionEquality().hash(nombreJugadores);

  @JsonKey(ignore: true)
  @override
  $PaginaNombrarJugadoresCopyWith<PaginaNombrarJugadores> get copyWith =>
      _$PaginaNombrarJugadoresCopyWithImpl<PaginaNombrarJugadores>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return paginanombrarjugadores(cantidadJugadores, nombreJugadores);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return paginanombrarjugadores?.call(cantidadJugadores, nombreJugadores);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginanombrarjugadores != null) {
      return paginanombrarjugadores(cantidadJugadores, nombreJugadores);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return paginanombrarjugadores(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return paginanombrarjugadores?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginanombrarjugadores != null) {
      return paginanombrarjugadores(this);
    }
    return orElse();
  }
}

abstract class PaginaNombrarJugadores implements Estado {
  const factory PaginaNombrarJugadores(
          int cantidadJugadores, List<String> nombreJugadores) =
      _$PaginaNombrarJugadores;

  int get cantidadJugadores => throw _privateConstructorUsedError;
  List<String> get nombreJugadores => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PaginaNombrarJugadoresCopyWith<PaginaNombrarJugadores> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PaginaRondaPrimeraCopyWith<$Res> {
  factory $PaginaRondaPrimeraCopyWith(
          PaginaRondaPrimera value, $Res Function(PaginaRondaPrimera) then) =
      _$PaginaRondaPrimeraCopyWithImpl<$Res>;
  $Res call({List<String> listaConJugadores, int ronda});
}

/// @nodoc
class _$PaginaRondaPrimeraCopyWithImpl<$Res> extends _$EstadoCopyWithImpl<$Res>
    implements $PaginaRondaPrimeraCopyWith<$Res> {
  _$PaginaRondaPrimeraCopyWithImpl(
      PaginaRondaPrimera _value, $Res Function(PaginaRondaPrimera) _then)
      : super(_value, (v) => _then(v as PaginaRondaPrimera));

  @override
  PaginaRondaPrimera get _value => super._value as PaginaRondaPrimera;

  @override
  $Res call({
    Object? listaConJugadores = freezed,
    Object? ronda = freezed,
  }) {
    return _then(PaginaRondaPrimera(
      listaConJugadores == freezed
          ? _value.listaConJugadores
          : listaConJugadores // ignore: cast_nullable_to_non_nullable
              as List<String>,
      ronda == freezed
          ? _value.ronda
          : ronda // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$PaginaRondaPrimera
    with DiagnosticableTreeMixin
    implements PaginaRondaPrimera {
  const _$PaginaRondaPrimera(this.listaConJugadores, this.ronda);

  @override
  final List<String> listaConJugadores;
  @override
  final int ronda;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.paginarondaprimera(listaConJugadores: $listaConJugadores, ronda: $ronda)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.paginarondaprimera'))
      ..add(DiagnosticsProperty('listaConJugadores', listaConJugadores))
      ..add(DiagnosticsProperty('ronda', ronda));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is PaginaRondaPrimera &&
            (identical(other.listaConJugadores, listaConJugadores) ||
                const DeepCollectionEquality()
                    .equals(other.listaConJugadores, listaConJugadores)) &&
            (identical(other.ronda, ronda) ||
                const DeepCollectionEquality().equals(other.ronda, ronda)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(listaConJugadores) ^
      const DeepCollectionEquality().hash(ronda);

  @JsonKey(ignore: true)
  @override
  $PaginaRondaPrimeraCopyWith<PaginaRondaPrimera> get copyWith =>
      _$PaginaRondaPrimeraCopyWithImpl<PaginaRondaPrimera>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return paginarondaprimera(listaConJugadores, ronda);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return paginarondaprimera?.call(listaConJugadores, ronda);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginarondaprimera != null) {
      return paginarondaprimera(listaConJugadores, ronda);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return paginarondaprimera(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return paginarondaprimera?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginarondaprimera != null) {
      return paginarondaprimera(this);
    }
    return orElse();
  }
}

abstract class PaginaRondaPrimera implements Estado {
  const factory PaginaRondaPrimera(List<String> listaConJugadores, int ronda) =
      _$PaginaRondaPrimera;

  List<String> get listaConJugadores => throw _privateConstructorUsedError;
  int get ronda => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PaginaRondaPrimeraCopyWith<PaginaRondaPrimera> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ErrorNombresJugadoresCopyWith<$Res> {
  factory $ErrorNombresJugadoresCopyWith(ErrorNombresJugadores value,
          $Res Function(ErrorNombresJugadores) then) =
      _$ErrorNombresJugadoresCopyWithImpl<$Res>;
  $Res call({String mensajeValidacion});
}

/// @nodoc
class _$ErrorNombresJugadoresCopyWithImpl<$Res>
    extends _$EstadoCopyWithImpl<$Res>
    implements $ErrorNombresJugadoresCopyWith<$Res> {
  _$ErrorNombresJugadoresCopyWithImpl(
      ErrorNombresJugadores _value, $Res Function(ErrorNombresJugadores) _then)
      : super(_value, (v) => _then(v as ErrorNombresJugadores));

  @override
  ErrorNombresJugadores get _value => super._value as ErrorNombresJugadores;

  @override
  $Res call({
    Object? mensajeValidacion = freezed,
  }) {
    return _then(ErrorNombresJugadores(
      mensajeValidacion == freezed
          ? _value.mensajeValidacion
          : mensajeValidacion // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ErrorNombresJugadores
    with DiagnosticableTreeMixin
    implements ErrorNombresJugadores {
  const _$ErrorNombresJugadores(this.mensajeValidacion);

  @override
  final String mensajeValidacion;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.errornombresjugadores(mensajeValidacion: $mensajeValidacion)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.errornombresjugadores'))
      ..add(DiagnosticsProperty('mensajeValidacion', mensajeValidacion));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ErrorNombresJugadores &&
            (identical(other.mensajeValidacion, mensajeValidacion) ||
                const DeepCollectionEquality()
                    .equals(other.mensajeValidacion, mensajeValidacion)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(mensajeValidacion);

  @JsonKey(ignore: true)
  @override
  $ErrorNombresJugadoresCopyWith<ErrorNombresJugadores> get copyWith =>
      _$ErrorNombresJugadoresCopyWithImpl<ErrorNombresJugadores>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return errornombresjugadores(mensajeValidacion);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return errornombresjugadores?.call(mensajeValidacion);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (errornombresjugadores != null) {
      return errornombresjugadores(mensajeValidacion);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return errornombresjugadores(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return errornombresjugadores?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (errornombresjugadores != null) {
      return errornombresjugadores(this);
    }
    return orElse();
  }
}

abstract class ErrorNombresJugadores implements Estado {
  const factory ErrorNombresJugadores(String mensajeValidacion) =
      _$ErrorNombresJugadores;

  String get mensajeValidacion => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ErrorNombresJugadoresCopyWith<ErrorNombresJugadores> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PaginaCartasJugadorR1CopyWith<$Res> {
  factory $PaginaCartasJugadorR1CopyWith(PaginaCartasJugadorR1 value,
          $Res Function(PaginaCartasJugadorR1) then) =
      _$PaginaCartasJugadorR1CopyWithImpl<$Res>;
  $Res call(
      {List<int> numeroDeCartas,
      String nombreJugador,
      int cantidadCartasRepartir,
      int ronda});
}

/// @nodoc
class _$PaginaCartasJugadorR1CopyWithImpl<$Res>
    extends _$EstadoCopyWithImpl<$Res>
    implements $PaginaCartasJugadorR1CopyWith<$Res> {
  _$PaginaCartasJugadorR1CopyWithImpl(
      PaginaCartasJugadorR1 _value, $Res Function(PaginaCartasJugadorR1) _then)
      : super(_value, (v) => _then(v as PaginaCartasJugadorR1));

  @override
  PaginaCartasJugadorR1 get _value => super._value as PaginaCartasJugadorR1;

  @override
  $Res call({
    Object? numeroDeCartas = freezed,
    Object? nombreJugador = freezed,
    Object? cantidadCartasRepartir = freezed,
    Object? ronda = freezed,
  }) {
    return _then(PaginaCartasJugadorR1(
      numeroDeCartas == freezed
          ? _value.numeroDeCartas
          : numeroDeCartas // ignore: cast_nullable_to_non_nullable
              as List<int>,
      nombreJugador == freezed
          ? _value.nombreJugador
          : nombreJugador // ignore: cast_nullable_to_non_nullable
              as String,
      cantidadCartasRepartir == freezed
          ? _value.cantidadCartasRepartir
          : cantidadCartasRepartir // ignore: cast_nullable_to_non_nullable
              as int,
      ronda == freezed
          ? _value.ronda
          : ronda // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$PaginaCartasJugadorR1
    with DiagnosticableTreeMixin
    implements PaginaCartasJugadorR1 {
  const _$PaginaCartasJugadorR1(this.numeroDeCartas, this.nombreJugador,
      this.cantidadCartasRepartir, this.ronda);

  @override
  final List<int> numeroDeCartas;
  @override
  final String nombreJugador;
  @override
  final int cantidadCartasRepartir;
  @override
  final int ronda;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.paginacartasjugadorr1(numeroDeCartas: $numeroDeCartas, nombreJugador: $nombreJugador, cantidadCartasRepartir: $cantidadCartasRepartir, ronda: $ronda)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.paginacartasjugadorr1'))
      ..add(DiagnosticsProperty('numeroDeCartas', numeroDeCartas))
      ..add(DiagnosticsProperty('nombreJugador', nombreJugador))
      ..add(
          DiagnosticsProperty('cantidadCartasRepartir', cantidadCartasRepartir))
      ..add(DiagnosticsProperty('ronda', ronda));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is PaginaCartasJugadorR1 &&
            (identical(other.numeroDeCartas, numeroDeCartas) ||
                const DeepCollectionEquality()
                    .equals(other.numeroDeCartas, numeroDeCartas)) &&
            (identical(other.nombreJugador, nombreJugador) ||
                const DeepCollectionEquality()
                    .equals(other.nombreJugador, nombreJugador)) &&
            (identical(other.cantidadCartasRepartir, cantidadCartasRepartir) ||
                const DeepCollectionEquality().equals(
                    other.cantidadCartasRepartir, cantidadCartasRepartir)) &&
            (identical(other.ronda, ronda) ||
                const DeepCollectionEquality().equals(other.ronda, ronda)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(numeroDeCartas) ^
      const DeepCollectionEquality().hash(nombreJugador) ^
      const DeepCollectionEquality().hash(cantidadCartasRepartir) ^
      const DeepCollectionEquality().hash(ronda);

  @JsonKey(ignore: true)
  @override
  $PaginaCartasJugadorR1CopyWith<PaginaCartasJugadorR1> get copyWith =>
      _$PaginaCartasJugadorR1CopyWithImpl<PaginaCartasJugadorR1>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return paginacartasjugadorr1(
        numeroDeCartas, nombreJugador, cantidadCartasRepartir, ronda);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return paginacartasjugadorr1?.call(
        numeroDeCartas, nombreJugador, cantidadCartasRepartir, ronda);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginacartasjugadorr1 != null) {
      return paginacartasjugadorr1(
          numeroDeCartas, nombreJugador, cantidadCartasRepartir, ronda);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return paginacartasjugadorr1(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return paginacartasjugadorr1?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginacartasjugadorr1 != null) {
      return paginacartasjugadorr1(this);
    }
    return orElse();
  }
}

abstract class PaginaCartasJugadorR1 implements Estado {
  const factory PaginaCartasJugadorR1(
      List<int> numeroDeCartas,
      String nombreJugador,
      int cantidadCartasRepartir,
      int ronda) = _$PaginaCartasJugadorR1;

  List<int> get numeroDeCartas => throw _privateConstructorUsedError;
  String get nombreJugador => throw _privateConstructorUsedError;
  int get cantidadCartasRepartir => throw _privateConstructorUsedError;
  int get ronda => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PaginaCartasJugadorR1CopyWith<PaginaCartasJugadorR1> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ErrorJugadorSeleccionadoCopyWith<$Res> {
  factory $ErrorJugadorSeleccionadoCopyWith(ErrorJugadorSeleccionado value,
          $Res Function(ErrorJugadorSeleccionado) then) =
      _$ErrorJugadorSeleccionadoCopyWithImpl<$Res>;
  $Res call({String msj});
}

/// @nodoc
class _$ErrorJugadorSeleccionadoCopyWithImpl<$Res>
    extends _$EstadoCopyWithImpl<$Res>
    implements $ErrorJugadorSeleccionadoCopyWith<$Res> {
  _$ErrorJugadorSeleccionadoCopyWithImpl(ErrorJugadorSeleccionado _value,
      $Res Function(ErrorJugadorSeleccionado) _then)
      : super(_value, (v) => _then(v as ErrorJugadorSeleccionado));

  @override
  ErrorJugadorSeleccionado get _value =>
      super._value as ErrorJugadorSeleccionado;

  @override
  $Res call({
    Object? msj = freezed,
  }) {
    return _then(ErrorJugadorSeleccionado(
      msj == freezed
          ? _value.msj
          : msj // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ErrorJugadorSeleccionado
    with DiagnosticableTreeMixin
    implements ErrorJugadorSeleccionado {
  const _$ErrorJugadorSeleccionado(this.msj);

  @override
  final String msj;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.errorjugadorseleccionado(msj: $msj)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.errorjugadorseleccionado'))
      ..add(DiagnosticsProperty('msj', msj));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ErrorJugadorSeleccionado &&
            (identical(other.msj, msj) ||
                const DeepCollectionEquality().equals(other.msj, msj)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(msj);

  @JsonKey(ignore: true)
  @override
  $ErrorJugadorSeleccionadoCopyWith<ErrorJugadorSeleccionado> get copyWith =>
      _$ErrorJugadorSeleccionadoCopyWithImpl<ErrorJugadorSeleccionado>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return errorjugadorseleccionado(msj);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return errorjugadorseleccionado?.call(msj);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (errorjugadorseleccionado != null) {
      return errorjugadorseleccionado(msj);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return errorjugadorseleccionado(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return errorjugadorseleccionado?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (errorjugadorseleccionado != null) {
      return errorjugadorseleccionado(this);
    }
    return orElse();
  }
}

abstract class ErrorJugadorSeleccionado implements Estado {
  const factory ErrorJugadorSeleccionado(String msj) =
      _$ErrorJugadorSeleccionado;

  String get msj => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ErrorJugadorSeleccionadoCopyWith<ErrorJugadorSeleccionado> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CargandoPartidaTerminadaCopyWith<$Res> {
  factory $CargandoPartidaTerminadaCopyWith(CargandoPartidaTerminada value,
          $Res Function(CargandoPartidaTerminada) then) =
      _$CargandoPartidaTerminadaCopyWithImpl<$Res>;
}

/// @nodoc
class _$CargandoPartidaTerminadaCopyWithImpl<$Res>
    extends _$EstadoCopyWithImpl<$Res>
    implements $CargandoPartidaTerminadaCopyWith<$Res> {
  _$CargandoPartidaTerminadaCopyWithImpl(CargandoPartidaTerminada _value,
      $Res Function(CargandoPartidaTerminada) _then)
      : super(_value, (v) => _then(v as CargandoPartidaTerminada));

  @override
  CargandoPartidaTerminada get _value =>
      super._value as CargandoPartidaTerminada;
}

/// @nodoc

class _$CargandoPartidaTerminada
    with DiagnosticableTreeMixin
    implements CargandoPartidaTerminada {
  const _$CargandoPartidaTerminada();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.cargandopartidaterminada()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.cargandopartidaterminada'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is CargandoPartidaTerminada);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return cargandopartidaterminada();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return cargandopartidaterminada?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (cargandopartidaterminada != null) {
      return cargandopartidaterminada();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return cargandopartidaterminada(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return cargandopartidaterminada?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (cargandopartidaterminada != null) {
      return cargandopartidaterminada(this);
    }
    return orElse();
  }
}

abstract class CargandoPartidaTerminada implements Estado {
  const factory CargandoPartidaTerminada() = _$CargandoPartidaTerminada;
}

/// @nodoc
abstract class $ErrorConexionPartidaTCopyWith<$Res> {
  factory $ErrorConexionPartidaTCopyWith(ErrorConexionPartidaT value,
          $Res Function(ErrorConexionPartidaT) then) =
      _$ErrorConexionPartidaTCopyWithImpl<$Res>;
  $Res call({String msj, String msj2});
}

/// @nodoc
class _$ErrorConexionPartidaTCopyWithImpl<$Res>
    extends _$EstadoCopyWithImpl<$Res>
    implements $ErrorConexionPartidaTCopyWith<$Res> {
  _$ErrorConexionPartidaTCopyWithImpl(
      ErrorConexionPartidaT _value, $Res Function(ErrorConexionPartidaT) _then)
      : super(_value, (v) => _then(v as ErrorConexionPartidaT));

  @override
  ErrorConexionPartidaT get _value => super._value as ErrorConexionPartidaT;

  @override
  $Res call({
    Object? msj = freezed,
    Object? msj2 = freezed,
  }) {
    return _then(ErrorConexionPartidaT(
      msj == freezed
          ? _value.msj
          : msj // ignore: cast_nullable_to_non_nullable
              as String,
      msj2 == freezed
          ? _value.msj2
          : msj2 // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ErrorConexionPartidaT
    with DiagnosticableTreeMixin
    implements ErrorConexionPartidaT {
  const _$ErrorConexionPartidaT(this.msj, this.msj2);

  @override
  final String msj;
  @override
  final String msj2;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.errorconexionpartidat(msj: $msj, msj2: $msj2)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.errorconexionpartidat'))
      ..add(DiagnosticsProperty('msj', msj))
      ..add(DiagnosticsProperty('msj2', msj2));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ErrorConexionPartidaT &&
            (identical(other.msj, msj) ||
                const DeepCollectionEquality().equals(other.msj, msj)) &&
            (identical(other.msj2, msj2) ||
                const DeepCollectionEquality().equals(other.msj2, msj2)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(msj) ^
      const DeepCollectionEquality().hash(msj2);

  @JsonKey(ignore: true)
  @override
  $ErrorConexionPartidaTCopyWith<ErrorConexionPartidaT> get copyWith =>
      _$ErrorConexionPartidaTCopyWithImpl<ErrorConexionPartidaT>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return errorconexionpartidat(msj, msj2);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return errorconexionpartidat?.call(msj, msj2);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (errorconexionpartidat != null) {
      return errorconexionpartidat(msj, msj2);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return errorconexionpartidat(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return errorconexionpartidat?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (errorconexionpartidat != null) {
      return errorconexionpartidat(this);
    }
    return orElse();
  }
}

abstract class ErrorConexionPartidaT implements Estado {
  const factory ErrorConexionPartidaT(String msj, String msj2) =
      _$ErrorConexionPartidaT;

  String get msj => throw _privateConstructorUsedError;
  String get msj2 => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ErrorConexionPartidaTCopyWith<ErrorConexionPartidaT> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PartidaTerminadaCopyWith<$Res> {
  factory $PartidaTerminadaCopyWith(
          PartidaTerminada value, $Res Function(PartidaTerminada) then) =
      _$PartidaTerminadaCopyWithImpl<$Res>;
  $Res call({List<dynamic> partidaTerminada});
}

/// @nodoc
class _$PartidaTerminadaCopyWithImpl<$Res> extends _$EstadoCopyWithImpl<$Res>
    implements $PartidaTerminadaCopyWith<$Res> {
  _$PartidaTerminadaCopyWithImpl(
      PartidaTerminada _value, $Res Function(PartidaTerminada) _then)
      : super(_value, (v) => _then(v as PartidaTerminada));

  @override
  PartidaTerminada get _value => super._value as PartidaTerminada;

  @override
  $Res call({
    Object? partidaTerminada = freezed,
  }) {
    return _then(PartidaTerminada(
      partidaTerminada == freezed
          ? _value.partidaTerminada
          : partidaTerminada // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
    ));
  }
}

/// @nodoc

class _$PartidaTerminada
    with DiagnosticableTreeMixin
    implements PartidaTerminada {
  const _$PartidaTerminada(this.partidaTerminada);

  @override
  final List<dynamic> partidaTerminada;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.partidaterminada(partidaTerminada: $partidaTerminada)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.partidaterminada'))
      ..add(DiagnosticsProperty('partidaTerminada', partidaTerminada));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is PartidaTerminada &&
            (identical(other.partidaTerminada, partidaTerminada) ||
                const DeepCollectionEquality()
                    .equals(other.partidaTerminada, partidaTerminada)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(partidaTerminada);

  @JsonKey(ignore: true)
  @override
  $PartidaTerminadaCopyWith<PartidaTerminada> get copyWith =>
      _$PartidaTerminadaCopyWithImpl<PartidaTerminada>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return partidaterminada(partidaTerminada);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return partidaterminada?.call(partidaTerminada);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (partidaterminada != null) {
      return partidaterminada(partidaTerminada);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return partidaterminada(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return partidaterminada?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (partidaterminada != null) {
      return partidaterminada(this);
    }
    return orElse();
  }
}

abstract class PartidaTerminada implements Estado {
  const factory PartidaTerminada(List<dynamic> partidaTerminada) =
      _$PartidaTerminada;

  List<dynamic> get partidaTerminada => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PartidaTerminadaCopyWith<PartidaTerminada> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PaginaMisPartidasCopyWith<$Res> {
  factory $PaginaMisPartidasCopyWith(
          PaginaMisPartidas value, $Res Function(PaginaMisPartidas) then) =
      _$PaginaMisPartidasCopyWithImpl<$Res>;
  $Res call({List<dynamic> misPartidas});
}

/// @nodoc
class _$PaginaMisPartidasCopyWithImpl<$Res> extends _$EstadoCopyWithImpl<$Res>
    implements $PaginaMisPartidasCopyWith<$Res> {
  _$PaginaMisPartidasCopyWithImpl(
      PaginaMisPartidas _value, $Res Function(PaginaMisPartidas) _then)
      : super(_value, (v) => _then(v as PaginaMisPartidas));

  @override
  PaginaMisPartidas get _value => super._value as PaginaMisPartidas;

  @override
  $Res call({
    Object? misPartidas = freezed,
  }) {
    return _then(PaginaMisPartidas(
      misPartidas == freezed
          ? _value.misPartidas
          : misPartidas // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
    ));
  }
}

/// @nodoc

class _$PaginaMisPartidas
    with DiagnosticableTreeMixin
    implements PaginaMisPartidas {
  const _$PaginaMisPartidas(this.misPartidas);

  @override
  final List<dynamic> misPartidas;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.paginamispartidas(misPartidas: $misPartidas)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.paginamispartidas'))
      ..add(DiagnosticsProperty('misPartidas', misPartidas));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is PaginaMisPartidas &&
            (identical(other.misPartidas, misPartidas) ||
                const DeepCollectionEquality()
                    .equals(other.misPartidas, misPartidas)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(misPartidas);

  @JsonKey(ignore: true)
  @override
  $PaginaMisPartidasCopyWith<PaginaMisPartidas> get copyWith =>
      _$PaginaMisPartidasCopyWithImpl<PaginaMisPartidas>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return paginamispartidas(misPartidas);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return paginamispartidas?.call(misPartidas);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginamispartidas != null) {
      return paginamispartidas(misPartidas);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return paginamispartidas(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return paginamispartidas?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (paginamispartidas != null) {
      return paginamispartidas(this);
    }
    return orElse();
  }
}

abstract class PaginaMisPartidas implements Estado {
  const factory PaginaMisPartidas(List<dynamic> misPartidas) =
      _$PaginaMisPartidas;

  List<dynamic> get misPartidas => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PaginaMisPartidasCopyWith<PaginaMisPartidas> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $VerDetallesPartidaCopyWith<$Res> {
  factory $VerDetallesPartidaCopyWith(
          VerDetallesPartida value, $Res Function(VerDetallesPartida) then) =
      _$VerDetallesPartidaCopyWithImpl<$Res>;
  $Res call({List<dynamic> detallePartida, int index});
}

/// @nodoc
class _$VerDetallesPartidaCopyWithImpl<$Res> extends _$EstadoCopyWithImpl<$Res>
    implements $VerDetallesPartidaCopyWith<$Res> {
  _$VerDetallesPartidaCopyWithImpl(
      VerDetallesPartida _value, $Res Function(VerDetallesPartida) _then)
      : super(_value, (v) => _then(v as VerDetallesPartida));

  @override
  VerDetallesPartida get _value => super._value as VerDetallesPartida;

  @override
  $Res call({
    Object? detallePartida = freezed,
    Object? index = freezed,
  }) {
    return _then(VerDetallesPartida(
      detallePartida == freezed
          ? _value.detallePartida
          : detallePartida // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
      index == freezed
          ? _value.index
          : index // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$VerDetallesPartida
    with DiagnosticableTreeMixin
    implements VerDetallesPartida {
  const _$VerDetallesPartida(this.detallePartida, this.index);

  @override
  final List<dynamic> detallePartida;
  @override
  final int index;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.verdetallespartida(detallePartida: $detallePartida, index: $index)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.verdetallespartida'))
      ..add(DiagnosticsProperty('detallePartida', detallePartida))
      ..add(DiagnosticsProperty('index', index));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is VerDetallesPartida &&
            (identical(other.detallePartida, detallePartida) ||
                const DeepCollectionEquality()
                    .equals(other.detallePartida, detallePartida)) &&
            (identical(other.index, index) ||
                const DeepCollectionEquality().equals(other.index, index)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(detallePartida) ^
      const DeepCollectionEquality().hash(index);

  @JsonKey(ignore: true)
  @override
  $VerDetallesPartidaCopyWith<VerDetallesPartida> get copyWith =>
      _$VerDetallesPartidaCopyWithImpl<VerDetallesPartida>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return verdetallespartida(detallePartida, index);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return verdetallespartida?.call(detallePartida, index);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (verdetallespartida != null) {
      return verdetallespartida(detallePartida, index);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return verdetallespartida(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return verdetallespartida?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (verdetallespartida != null) {
      return verdetallespartida(this);
    }
    return orElse();
  }
}

abstract class VerDetallesPartida implements Estado {
  const factory VerDetallesPartida(List<dynamic> detallePartida, int index) =
      _$VerDetallesPartida;

  List<dynamic> get detallePartida => throw _privateConstructorUsedError;
  int get index => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $VerDetallesPartidaCopyWith<VerDetallesPartida> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EstadisticaJugadorMostradaCopyWith<$Res> {
  factory $EstadisticaJugadorMostradaCopyWith(EstadisticaJugadorMostrada value,
          $Res Function(EstadisticaJugadorMostrada) then) =
      _$EstadisticaJugadorMostradaCopyWithImpl<$Res>;
  $Res call({List<dynamic> lista, int indice, int index});
}

/// @nodoc
class _$EstadisticaJugadorMostradaCopyWithImpl<$Res>
    extends _$EstadoCopyWithImpl<$Res>
    implements $EstadisticaJugadorMostradaCopyWith<$Res> {
  _$EstadisticaJugadorMostradaCopyWithImpl(EstadisticaJugadorMostrada _value,
      $Res Function(EstadisticaJugadorMostrada) _then)
      : super(_value, (v) => _then(v as EstadisticaJugadorMostrada));

  @override
  EstadisticaJugadorMostrada get _value =>
      super._value as EstadisticaJugadorMostrada;

  @override
  $Res call({
    Object? lista = freezed,
    Object? indice = freezed,
    Object? index = freezed,
  }) {
    return _then(EstadisticaJugadorMostrada(
      lista == freezed
          ? _value.lista
          : lista // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
      indice == freezed
          ? _value.indice
          : indice // ignore: cast_nullable_to_non_nullable
              as int,
      index == freezed
          ? _value.index
          : index // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$EstadisticaJugadorMostrada
    with DiagnosticableTreeMixin
    implements EstadisticaJugadorMostrada {
  const _$EstadisticaJugadorMostrada(this.lista, this.indice, this.index);

  @override
  final List<dynamic> lista;
  @override
  final int indice;
  @override
  final int index;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.estadisticajugadormostrada(lista: $lista, indice: $indice, index: $index)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.estadisticajugadormostrada'))
      ..add(DiagnosticsProperty('lista', lista))
      ..add(DiagnosticsProperty('indice', indice))
      ..add(DiagnosticsProperty('index', index));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is EstadisticaJugadorMostrada &&
            (identical(other.lista, lista) ||
                const DeepCollectionEquality().equals(other.lista, lista)) &&
            (identical(other.indice, indice) ||
                const DeepCollectionEquality().equals(other.indice, indice)) &&
            (identical(other.index, index) ||
                const DeepCollectionEquality().equals(other.index, index)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(lista) ^
      const DeepCollectionEquality().hash(indice) ^
      const DeepCollectionEquality().hash(index);

  @JsonKey(ignore: true)
  @override
  $EstadisticaJugadorMostradaCopyWith<EstadisticaJugadorMostrada>
      get copyWith =>
          _$EstadisticaJugadorMostradaCopyWithImpl<EstadisticaJugadorMostrada>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return estadisticajugadormostrada(lista, indice, index);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return estadisticajugadormostrada?.call(lista, indice, index);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (estadisticajugadormostrada != null) {
      return estadisticajugadormostrada(lista, indice, index);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return estadisticajugadormostrada(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return estadisticajugadormostrada?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (estadisticajugadormostrada != null) {
      return estadisticajugadormostrada(this);
    }
    return orElse();
  }
}

abstract class EstadisticaJugadorMostrada implements Estado {
  const factory EstadisticaJugadorMostrada(
          List<dynamic> lista, int indice, int index) =
      _$EstadisticaJugadorMostrada;

  List<dynamic> get lista => throw _privateConstructorUsedError;
  int get indice => throw _privateConstructorUsedError;
  int get index => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $EstadisticaJugadorMostradaCopyWith<EstadisticaJugadorMostrada>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EliminandoPartidaCopyWith<$Res> {
  factory $EliminandoPartidaCopyWith(
          EliminandoPartida value, $Res Function(EliminandoPartida) then) =
      _$EliminandoPartidaCopyWithImpl<$Res>;
  $Res call({int indice});
}

/// @nodoc
class _$EliminandoPartidaCopyWithImpl<$Res> extends _$EstadoCopyWithImpl<$Res>
    implements $EliminandoPartidaCopyWith<$Res> {
  _$EliminandoPartidaCopyWithImpl(
      EliminandoPartida _value, $Res Function(EliminandoPartida) _then)
      : super(_value, (v) => _then(v as EliminandoPartida));

  @override
  EliminandoPartida get _value => super._value as EliminandoPartida;

  @override
  $Res call({
    Object? indice = freezed,
  }) {
    return _then(EliminandoPartida(
      indice == freezed
          ? _value.indice
          : indice // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$EliminandoPartida
    with DiagnosticableTreeMixin
    implements EliminandoPartida {
  const _$EliminandoPartida(this.indice);

  @override
  final int indice;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.eliminandopartida(indice: $indice)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.eliminandopartida'))
      ..add(DiagnosticsProperty('indice', indice));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is EliminandoPartida &&
            (identical(other.indice, indice) ||
                const DeepCollectionEquality().equals(other.indice, indice)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(indice);

  @JsonKey(ignore: true)
  @override
  $EliminandoPartidaCopyWith<EliminandoPartida> get copyWith =>
      _$EliminandoPartidaCopyWithImpl<EliminandoPartida>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return eliminandopartida(indice);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return eliminandopartida?.call(indice);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (eliminandopartida != null) {
      return eliminandopartida(indice);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return eliminandopartida(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return eliminandopartida?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (eliminandopartida != null) {
      return eliminandopartida(this);
    }
    return orElse();
  }
}

abstract class EliminandoPartida implements Estado {
  const factory EliminandoPartida(int indice) = _$EliminandoPartida;

  int get indice => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $EliminandoPartidaCopyWith<EliminandoPartida> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PartidaEliminadaCopyWith<$Res> {
  factory $PartidaEliminadaCopyWith(
          PartidaEliminada value, $Res Function(PartidaEliminada) then) =
      _$PartidaEliminadaCopyWithImpl<$Res>;
  $Res call({String mensaje});
}

/// @nodoc
class _$PartidaEliminadaCopyWithImpl<$Res> extends _$EstadoCopyWithImpl<$Res>
    implements $PartidaEliminadaCopyWith<$Res> {
  _$PartidaEliminadaCopyWithImpl(
      PartidaEliminada _value, $Res Function(PartidaEliminada) _then)
      : super(_value, (v) => _then(v as PartidaEliminada));

  @override
  PartidaEliminada get _value => super._value as PartidaEliminada;

  @override
  $Res call({
    Object? mensaje = freezed,
  }) {
    return _then(PartidaEliminada(
      mensaje == freezed
          ? _value.mensaje
          : mensaje // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$PartidaEliminada
    with DiagnosticableTreeMixin
    implements PartidaEliminada {
  const _$PartidaEliminada(this.mensaje);

  @override
  final String mensaje;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado.partidaeliminada(mensaje: $mensaje)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado.partidaeliminada'))
      ..add(DiagnosticsProperty('mensaje', mensaje));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is PartidaEliminada &&
            (identical(other.mensaje, mensaje) ||
                const DeepCollectionEquality().equals(other.mensaje, mensaje)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(mensaje);

  @JsonKey(ignore: true)
  @override
  $PartidaEliminadaCopyWith<PartidaEliminada> get copyWith =>
      _$PartidaEliminadaCopyWithImpl<PartidaEliminada>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(String nombre, String contrasena) iniciandome,
    required TResult Function() paginaprincipal,
    required TResult Function() paginaregistro,
    required TResult Function(String nombre, String contrasena) registrandome,
    required TResult Function(String mensaje) paginaerroracceso,
    required TResult Function(String mensajeRegistro) paginarespregistro,
    required TResult Function() paginanuevapartida,
    required TResult Function(
            int cantidadJugadores, List<String> nombreJugadores)
        paginanombrarjugadores,
    required TResult Function(List<String> listaConJugadores, int ronda)
        paginarondaprimera,
    required TResult Function(String mensajeValidacion) errornombresjugadores,
    required TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)
        paginacartasjugadorr1,
    required TResult Function(String msj) errorjugadorseleccionado,
    required TResult Function() cargandopartidaterminada,
    required TResult Function(String msj, String msj2) errorconexionpartidat,
    required TResult Function(List<dynamic> partidaTerminada) partidaterminada,
    required TResult Function(List<dynamic> misPartidas) paginamispartidas,
    required TResult Function(List<dynamic> detallePartida, int index)
        verdetallespartida,
    required TResult Function(List<dynamic> lista, int indice, int index)
        estadisticajugadormostrada,
    required TResult Function(int indice) eliminandopartida,
    required TResult Function(String mensaje) partidaeliminada,
  }) {
    return partidaeliminada(mensaje);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
  }) {
    return partidaeliminada?.call(mensaje);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(String nombre, String contrasena)? iniciandome,
    TResult Function()? paginaprincipal,
    TResult Function()? paginaregistro,
    TResult Function(String nombre, String contrasena)? registrandome,
    TResult Function(String mensaje)? paginaerroracceso,
    TResult Function(String mensajeRegistro)? paginarespregistro,
    TResult Function()? paginanuevapartida,
    TResult Function(int cantidadJugadores, List<String> nombreJugadores)?
        paginanombrarjugadores,
    TResult Function(List<String> listaConJugadores, int ronda)?
        paginarondaprimera,
    TResult Function(String mensajeValidacion)? errornombresjugadores,
    TResult Function(List<int> numeroDeCartas, String nombreJugador,
            int cantidadCartasRepartir, int ronda)?
        paginacartasjugadorr1,
    TResult Function(String msj)? errorjugadorseleccionado,
    TResult Function()? cargandopartidaterminada,
    TResult Function(String msj, String msj2)? errorconexionpartidat,
    TResult Function(List<dynamic> partidaTerminada)? partidaterminada,
    TResult Function(List<dynamic> misPartidas)? paginamispartidas,
    TResult Function(List<dynamic> detallePartida, int index)?
        verdetallespartida,
    TResult Function(List<dynamic> lista, int indice, int index)?
        estadisticajugadormostrada,
    TResult Function(int indice)? eliminandopartida,
    TResult Function(String mensaje)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (partidaeliminada != null) {
      return partidaeliminada(mensaje);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Login value) $default, {
    required TResult Function(Iniciandome value) iniciandome,
    required TResult Function(PaginaPrincipal value) paginaprincipal,
    required TResult Function(PaginaRegistro value) paginaregistro,
    required TResult Function(Registrandome value) registrandome,
    required TResult Function(PaginaErrorAcceso value) paginaerroracceso,
    required TResult Function(PaginaRespRegistro value) paginarespregistro,
    required TResult Function(PaginaNuevaPartida value) paginanuevapartida,
    required TResult Function(PaginaNombrarJugadores value)
        paginanombrarjugadores,
    required TResult Function(PaginaRondaPrimera value) paginarondaprimera,
    required TResult Function(ErrorNombresJugadores value)
        errornombresjugadores,
    required TResult Function(PaginaCartasJugadorR1 value)
        paginacartasjugadorr1,
    required TResult Function(ErrorJugadorSeleccionado value)
        errorjugadorseleccionado,
    required TResult Function(CargandoPartidaTerminada value)
        cargandopartidaterminada,
    required TResult Function(ErrorConexionPartidaT value)
        errorconexionpartidat,
    required TResult Function(PartidaTerminada value) partidaterminada,
    required TResult Function(PaginaMisPartidas value) paginamispartidas,
    required TResult Function(VerDetallesPartida value) verdetallespartida,
    required TResult Function(EstadisticaJugadorMostrada value)
        estadisticajugadormostrada,
    required TResult Function(EliminandoPartida value) eliminandopartida,
    required TResult Function(PartidaEliminada value) partidaeliminada,
  }) {
    return partidaeliminada(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
  }) {
    return partidaeliminada?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Login value)? $default, {
    TResult Function(Iniciandome value)? iniciandome,
    TResult Function(PaginaPrincipal value)? paginaprincipal,
    TResult Function(PaginaRegistro value)? paginaregistro,
    TResult Function(Registrandome value)? registrandome,
    TResult Function(PaginaErrorAcceso value)? paginaerroracceso,
    TResult Function(PaginaRespRegistro value)? paginarespregistro,
    TResult Function(PaginaNuevaPartida value)? paginanuevapartida,
    TResult Function(PaginaNombrarJugadores value)? paginanombrarjugadores,
    TResult Function(PaginaRondaPrimera value)? paginarondaprimera,
    TResult Function(ErrorNombresJugadores value)? errornombresjugadores,
    TResult Function(PaginaCartasJugadorR1 value)? paginacartasjugadorr1,
    TResult Function(ErrorJugadorSeleccionado value)? errorjugadorseleccionado,
    TResult Function(CargandoPartidaTerminada value)? cargandopartidaterminada,
    TResult Function(ErrorConexionPartidaT value)? errorconexionpartidat,
    TResult Function(PartidaTerminada value)? partidaterminada,
    TResult Function(PaginaMisPartidas value)? paginamispartidas,
    TResult Function(VerDetallesPartida value)? verdetallespartida,
    TResult Function(EstadisticaJugadorMostrada value)?
        estadisticajugadormostrada,
    TResult Function(EliminandoPartida value)? eliminandopartida,
    TResult Function(PartidaEliminada value)? partidaeliminada,
    required TResult orElse(),
  }) {
    if (partidaeliminada != null) {
      return partidaeliminada(this);
    }
    return orElse();
  }
}

abstract class PartidaEliminada implements Estado {
  const factory PartidaEliminada(String mensaje) = _$PartidaEliminada;

  String get mensaje => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PartidaEliminadaCopyWith<PartidaEliminada> get copyWith =>
      throw _privateConstructorUsedError;
}
